const exampleForm = {
    template : `
        <form class="col s12" action="#" method="post" name="form" @submit.prevent="saveData">
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input v-model="firstName" id="first_name" type="text" class="validate">
                    <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input v-model="lastName" id="last_name" type="text" class="validate">
                    <label for="last_name">Last Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">email</i>
                    <input v-model="email" id="email" type="email" class="validate">
                    <label for="email">Email</label>
                </div>
                <div class="input-field col s5">
                    <i class="material-icons prefix">phone</i>
                    <input v-model="telephone" id="telephone" type="tel" class="validate">
                    <label for="telephone">Telephone</label>
                </div>
                <div class="input-field col s1">
                    <button class="waves-effect waves-light btn"><i class="material-icons">add</i></button>
                </div>
            </div>
        </form>
    `,
    // props: [ 'firstName', 'lastName' ],
    // emits: [ 'firstName', 'lastName' ],
    data() {
        return {
            firstName: 'Luis',
            lastName: 'Dias',
            email: 'email@gmail',
            telephone: '1234567',
        }
    },
    methods: {
        saveData() {
            console.log(this.firstName);
            console.log(this.lastName);
            console.log(this.email);
            console.log(this.telephone);
        }
    },
}

app.component('example-form', exampleForm);