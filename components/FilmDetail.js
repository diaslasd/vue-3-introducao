const filmDetail = {
    template: `
<div class="col s3" :id="'film-' + index" v-show="filmShow">
    <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" :src="film.image" :alt="film.title" :title="film.title" >
        </div>
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">Descrição<i class="material-icons right">more_vert</i></span>
            <p class="center-align">
                <span>Nota <span :class="[ filmTop ? 'blue-text': 'red-text']">{{ film.stars}}</span></span>
            </p>
        </div>
        <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">Descrição<i class="material-icons right">close</i></span>
            <span>
                {{ film.description }}
            </span>
        </div>
        <div class="card-action center-align">
            <label>
                <input type="checkbox" v-model="film.select" v-on:change="addCart(film.id,film.select)"/>
                <span>Comprar</span>
            </label>
        </div>
    </div>
</div>
    `,
    // props: ['film'], // simples
    props: {
        film :{
            type: Object,
            required: true
        },
        index :{
            type: Number,
            required: true
        },
        filmTop :{
            type: Boolean,
            default: true
        },
        filmShow :{
            type: Boolean,
            default: true,
        },
    }, // detalhado
    emits: ['add-cart'],
    data() {
        return {
        }
    },

    methods: {
        addCart(item, select) {
            this.$emit('add-cart',item, select)
        }
    },
}

app.component('film-detail', filmDetail);