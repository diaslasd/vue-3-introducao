// document.addEventListener('DOMContentLoaded', function() {
//     var elems = document.querySelectorAll('select');
//     var instances = M.FormSelect.init(elems, '');
// }); // resolver pane do materialize com o select


const myApp = {
    data() {
        return {
            title: 'Curso Vue 3',
            logo: './assets/images/logo.png',
            name: 'Luis',
            lastName: 'Dias',
            now: new Date().toLocaleString(),
            nota: 6.9,
            styles : [
                    {
                        name : 'light',
                        color: {color:'#000', backgroundColor:'#FFF'}
                    },
                    {
                        name :'dark',
                        color: {color:'#FFF', backgroundColor:'#000'}
                    },
                    {
                        name :'blue',
                        color: {color:'#0000FF', backgroundColor:'#00AAFF'}
                    },
                    {
                        name :'blueDark',
                        color: {color:'#00AAFF', backgroundColor:'#0000AF'}
                    },
                ],
            styleSelect: 0,
            styleColor: {color:'#000', backgroundColor:'#FFF'},
            films: [
                {
                    id: 1,
                    title: 'Patrulha Canina',
                    description: 'O Filme, o filhote Ryder e seus amigos têm um grande desafio: Impedir o novo Prefeito de Adventure City, Humdinger, de causar muitos problemas.',
                    image: './assets/images/filme01.jpg',
                    stars: 3.4,
                    show: true,
                    select: false
                },
                {
                    id: 2,
                    title: 'Crud 2',
                    description: 'Uma Nova Era, em busca de um habitat mais seguro, os Croods descobrem um paraíso que atende todas as suas necessidades.',
                    image: './assets/images/filme02.jpg',
                    stars: 4.1,
                    show: true,
                    select: false
                },
                {
                    id: 3,
                    title: 'Tom & Jerry',
                    description: 'O Filme mostra uma das rivalidades mais amadas da história que é reacendida quando Jerry se muda para o melhor hotel de Nova York...',
                    image: './assets/images/filme03.jpg',
                    stars: 3.6,
                    show: false,
                    select: false
                },
                {
                    id: 4,
                    title: 'Fátima',
                    description: 'Fatima cria sozinha as duas filhas: Souad, de 15 anos, adolescente rebelde; e Nesrine, de 18 anos, começando os estudos de medicina.',
                    image: './assets/images/filme04.jpg',
                    stars: 3.6,
                    show: true,
                    select: false
                },
            ],
            cart: [],
        }
    },
    mounted() {
        setInterval( () => {
            this.now = new Date().toLocaleString()
            this.styleColor = this.styles[this.styleSelect].color;
        }, 1000);
    },
    computed: {
        fullName () {
            return this.name + ' ' + this.lastName;
        }
    },
    methods: {
        addCart(item, select) {
            if (select) {
                this.cart.push(item)
                console.log('adicionado ' + item)                
            } else {
                let index = this.cart.indexOf(item);
                this.cart.splice(index,1);
                console.log('removido ' + item);
            }
            console.log(this.cart.toString());
        },        
    },
}

const app = Vue.createApp(myApp);



