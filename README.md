# ![](./assets/images/icon.png) Introdução ao VUE 3
+ [ESPECIALIZA TI](https://academy.especializati.com.br/curso/introducao-ao-vue-js-3)
+ [Youtube](https://www.youtube.com/playlist?list=PLVSNL1PHDWvRbZsrcQ249Ae5MoKrlBTdd)

### Instalação via CDN
~~~html
<head>
  <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- material-icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>    
</head>  
<body>
    

  <!-- Vue 3 CDN -->
  <script src="https://unpkg.com/vue@next"></script>
</body>
~~~

### Menu
1. [Declarative Rendering](#declarative-rendering)
1. [Bind](#bind)
    1. [v-bind](#v-bind)
    1. [v-model](#v-model)
    1. [v-if](#v-if)
    1. [v-show](#v-show)
    1. [v-for](#v-for)
1. [Eventos](#v-eventos)
    1. [v-on](#v-on)
    1. [Manipulatores de eventos](#manipulatores-de-eventos)
1. [Funções e Metodos](#funções-e-metodos)
    1. [mounted](#mounted)
    1. [methods](#methods)
    1. [computed](#computed)
1. [Styles](#styles)
1. [Componentes](#componentes)

### Declarative Rendering
+ HTML
~~~html
<div id="app">
  Counter: {{ counter }}
</div>
~~~
+ Js
~~~js
const App = {
  data() {
    return {
      counter: 0
    }
  }
}

Vue.createApp(App).mount('#app')
~~~
[Topo](#introdução-ao-vue-3)
### Bind
#### v-bind 
~~~html
<!-- full syntax -->
<a v-bind:href="url"> ... </a>

<!-- shorthand -->
<a :href="url"> ... </a>

<!-- shorthand with dynamic argument -->
<a :[key]="url"> ... </a>
~~~
[Topo](#introdução-ao-vue-3)

#### v-model 
~~~html
<div id="two-way-binding">
  <p>{{ message }}</p>
  <input v-model="message" />
</div>
~~~
[Topo](#introdução-ao-vue-3)

#### v-if
~~~html
<div id="conditional-rendering">
  <span v-if="seen">Now you see me</span>
</div>
<div id="provaFinal">
  <span v-if="nota >= 7">Aprovado com louvor</span>
  <span v-else-if="nota >= 5">Aprovado</span>
  <span v-else >Reprovado</span>
</div>
~~~
[Topo](#introdução-ao-vue-3)

#### v-show
> style: display
~~~html
<div id="conditional-rendering">
  <span v-show="seen">Now you see me</span>
</div>
~~~
[Topo](#introdução-ao-vue-3)

#### v-for
~~~html
<div id="list-rendering">
  <ol>
    <li v-for="todo in todos">
      {{ todo.text }}
    </li>
  </ol>
</div>
~~~
[Topo](#introdução-ao-vue-3)
### Eventos
#### v-on
~~~html
<input type="checkbox" v-model="film.select" v-on:change="addCart(film.id,film.select)"/>

<!-- full syntax -->
<a v-on:click="doSomething"> ... </a>

<!-- shorthand -->
<a @click="doSomething"> ... </a>

<!-- shorthand with dynamic argument -->
<a @[event]="doSomething"> ... </a>
~~~
[Topo](#introdução-ao-vue-3)

#### Manipulatores de eventos
~~~html
<!-- a propagação do evento click será interrompida -->
<a v-on:click.stop="doThis"></a>

<!-- o evento submit deixará de recarregar a página -->
<form v-on:submit.prevent="onSubmit"></form>

<!-- modificadores podem ser encadeados -->
<a v-on:click.stop.prevent="doThat"></a>

<!-- é possível utilizar apenas o modificador -->
<form v-on:submit.prevent></form>

<!-- usar modo de captura ao adicionar o evento -->
<!-- ou seja, um evento em um elemento interno é tratado aqui após ser tratado por aquele elemento -->
<div v-on:click.capture="doThis">...</div>

<!-- só aciona o manipulador se event.target é o próprio elemento -->
<!-- isto é, não aciona a partir de um elemento filho -->
<div v-on:click.self="doThat">...</div>
~~~
[Topo](#introdução-ao-vue-3)
### Funções e Metodos
#### mounted
~~~js
mounted() {
        setInterval( () => {
            this.now = new Date().toLocaleString()
        }, 1000)
    },
~~~
[Topo](#introdução-ao-vue-3)

#### methods
~~~js
methods: {
        addCart(item, select) {
            if (select) {
                this.cart.push(item)
                console.log('adicionado ' + item)                
            } else {
                let index = this.cart.indexOf(item);
                this.cart.splice(index,1);
                console.log('removido ' + item);
            }
            console.log(this.cart.toString());
        },        
    },
~~~
[Topo](#introdução-ao-vue-3)

#### computed
~~~js
computed: {
        fullName () {
            return this.name + ' ' + this.lastName;
        }
    },
~~~
[Topo](#introdução-ao-vue-3)

### Styles
#### Style
~~~html
<div id="principal" :style="styleColor" >
~~~
~~~js
styleColor: {color:'#000', backgroundColor:'#FFF'},
~~~
[Topo](#introdução-ao-vue-3)
### Componentes
~~~js
// app.js
const app = Vue.createApp(myApp);
// FilmDetail.js
app.component('film-detail', filmDetail);
// index.html
app.mount('#app');
~~~
#### props
~~~html
<film-detail 
    :film="film"
    :index="index"
    :film-show="film.show"
    :film-top="film.stars > 4"
    @add-cart="addCart">
</film-detail>
~~~
~~~js
// props: ['film','index','filmTop','filmShow'], // simples
props: {
    film :{
        type: Object,
        required: true
    },
    index :{
        type: Number,
        required: true
    },
    filmTop :{
        type: Boolean,
        default: true
    },
    filmShow :{
        type: Boolean,
        default: true,
    },
}, // detalhado
~~~
[Topo](#introdução-ao-vue-3)
#### emits
~~~js
emits: ['add-cart'],
methods: {
    addCart(item, select) {
        this.$emit('add-cart',item, select)
    }
},
~~~
[Topo](#introdução-ao-vue-3)
### Form
[Topo](#introdução-ao-vue-3)
